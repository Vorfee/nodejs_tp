const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PhotoSchema = new Schema({
    description: {type: String, required: true, max: 100},
    price: {type: Number, required: true},
    link: {type: String, require: true},
});

module.exports = mongoose.model('Photos', PhotoSchema);