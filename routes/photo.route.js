const express = require('express');
const router = express.Router();

const photo_controller = require('../controllers/photo.controller');

router.get('/test', photo_controller.test);

router.post('/create', photo_controller.photo_create);

router.get('/:id', photo_controller.photo_details);

router.put('/:id/update', photo_controller.photo_update);

router.delete('/:id/delete', photo_controller.photo_delete);

module.exports = router;