/* Express / NodeJS / Mango 

Site de stream de photo

Node avec Express : 
    - Home page / view
    - Button stock de photo dans la base mango
    - CRUD base photo

http://localhost:3001/photos/test

MVC, photo.controller.js / photo.route.js / photo.model.js et app.js 

*/

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const photo = require('./routes/photo.route');
const mongoose = require('mongoose');

const http = require('http');
const path = require('path');

app.use('/photos', photo);

let port = 3001;

app.listen(port, () => {
    console.log('Serveur au port ' + port);
});

let MongoClient = require('mongodb').MongoClient;

MongoClient.connect("mongodb://localhost:3001/", function (err, db) {

    collection.insert({ id: 1, description: 'Une photo de test', price: '201', link: 'urldetest.com'});

    collection.find().toArray(function(err, items) {
        if(err) throw err;    
        console.log(items);            
    });
   
     if(err) throw err;
                
});

app.set('appName', 'Photos');
app.set('port', process.env.PORT || port);
app.set('views', path.join(__dirname, 'views'));

app.all('*', (req, res) => {
    res.render('index', {msg: 'Bienvenue chez Photos Delire'})
})

http .createServer(app)
    .listen(
        app.get('port'),
        () => {
            console.log(`Express.js serveur ecoutes sur le port ${app.
            get('port')}`)
        } );

/*

Test MongoDB avec ODM mangoose

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://abcd1234:abcd1234@cluster0.axf7h.mongodb.net/NodeJS?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true });

let dev_db_url = "abcd1234:abcd1234@cluster0.axf7h.mongodb.net/NodeJS?retryWrites=true&w=majority"
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

client.connect(err => {
  const collection = client.db("test").collection("devices");
  client.close();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
*/