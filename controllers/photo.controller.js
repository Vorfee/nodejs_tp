const Photo = require('../models/photo.model');

exports.test = function (req, res) {
    res.send('Test photo.controller!');
};

exports.photo_create = function (req, res) {
    let photo = new Photo(
        {
            description: req.body.description,
            price: req.body.price
        }
    )

    photo.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Photo stored successfully')
    })
};

exports.photo_details = function (req, res) {
    Photo.findById(req.params.id, function (err, photo) {
        if (err) return next(err);
        res.send(photo);
    })
};

exports.photo_update = function (req, res) {
    Photo.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, photo) {
        if (err) return next(err);
        res.send('Photo udpated.');
    });
};

exports.photo_delete = function (req, res) {
    Photo.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};